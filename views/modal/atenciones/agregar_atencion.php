<div id="Agregar" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="datos_atencion">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">AGREGAR ATENCION</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-user"></i></span>
                                <input type="text" class="form-control input-lg" name="paciente" id="paciente"
                                    readonly>
                            </div>
                            <input type="hidden" id="id" name="id">

                        </div>

                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                                <input type="text" class="form-control input-lg" name="profesional" id="profesional"
                                    readonly>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-heartbeat"></i></span>
                                <input type="text" class="form-control input-lg" name="diagnostico" id="diagnostico"
                                    placeholder="Ingresar diagnostico" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa fa-heart"></i></span>
                                <input type="text" class="form-control input-lg" name="tratamiento" id="tratamiento"
                                    placeholder="Ingresar tratamiento" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-money"></i></span>
                                <input type="number" class="form-control input-lg" name="precio" id="precio"
                                    placeholder="Ingresar precio" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-ellipsis-h"></i></span>
                                <select class="form-control input-lg" name="estado" id="estado">
                                    <option value="PAGADO">PAGADO</option>
                                    <option value="DEBE">DEBE</option>  
                                    <option value="PROMO">PROMO</option>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </form>
        </div>
    </div>
</div>