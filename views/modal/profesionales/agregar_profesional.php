<div id="Agregar" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="datos_profesional">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">AGREGAR PROFESIONAL</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                                <input type="text" class="form-control input-lg" name="nombresp" id="nombresp"
                                    placeholder="Ingresar nombre" required>
                            </div>
                        </div>
                        <div class="form-group"> 
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                                <input type="number" class="form-control input-lg" name="cedula" id="cedula"
                                    placeholder="Ingresar cédula" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                <input type="email" class="form-control input-lg" name="email" id="email"
                                    placeholder="Ingresar email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-id-badge"></i></span>
                                <input type="number" class="form-control input-lg" name="telefono" id="telefono"
                                    placeholder="Ingresar teléfono" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-window-restore"></i></span>
                                <select class="form-control input-lg" name="id_categoria" id="id_categoria">
                                    <?php require_once 'config/conexion.php';
                                        $sql = "SELECT * FROM categorias";
                                        $query = mysqli_query($con, $sql);

                                        while ($data = mysqli_fetch_assoc($query)){
                                            $id_categoria = $data['id_categoria'];
                                            $nombre = $data['nombre'];  
                                    ?>
                                    <option value="<?php echo $id_categoria; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </form>
        </div>
    </div>
</div>