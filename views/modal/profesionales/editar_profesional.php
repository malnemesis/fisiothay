<div id="editar" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="editar_profesional">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">EDITAR PROFESIONAL</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                                <input type="text" class="form-control input-lg" name="mod_nombres" id="mod_nombres"
                                    placeholder="Editar nombre" required>
                                <input type="hidden" id="mod_codigo" name="mod_codigo">
                                <input type="hidden" id="mod_id" name="mod_id">
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card "></i></span>
                                <input type="number" class="form-control input-lg" name="mod_cedula" id="mod_cedula"
                                    placeholder="Editar cedula" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-at"></i></span>
                                <input type="email" class="form-control input-lg" name="mod_email" id="mod_email"
                                    placeholder="Editar email" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-id-badge"></i></span>
                                <input type="number" class="form-control input-lg" name="mod_telefono" id="mod_telefono"
                                    placeholder="Editar telefono" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-window-restore"></i></span>
                                <select class="form-control input-lg" name="mod_id_categoria" id="mod_id_categoria">
                                    <?php require_once 'config/conexion.php';
                                        $sql = "SELECT * FROM categorias";
                                        $query = mysqli_query($con, $sql);

                                        while ($data = mysqli_fetch_assoc($query)){
                                            $id_categoria = $data['id_categoria'];
                                            $nombre = $data['nombre'];  
                                    ?>
                                    <option value="<?php echo $id_categoria; ?>"><?php echo $nombre; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </form>
        </div>
    </div>
</div>