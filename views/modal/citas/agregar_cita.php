<div id="ModalAdd" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="datos_cita">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">AGREGAR CITA</h4>
                </div>
                <div class="modal-body">
                    <div class="box-body">  
                    <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-users"></i></span>
                                <select class="form-control input-lg" name="id_paciente" id="id_paciente">
                                <option value="">Paciente</option>
                                    <?php require_once 'config/conexion.php';
                                        $sql = "SELECT * FROM pacientes";
                                        $query = mysqli_query($con, $sql);

                                        while ($data = mysqli_fetch_assoc($query)){
                                            $id_paciente = $data['id_paciente'];
                                            $nombres = $data['nombres'];  
                                    ?>
                                    <option value="<?php echo $id_paciente; ?>"><?php echo $nombres; ?></option>
                                    <?php
                                        } 
                                    ?>
                                </select>
                            </div>
                    
                        </div>
                            <div class="input-group">
                                <input type="hidden" name="start" id="start">
                            </div>
                    
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-address-card"></i></span>
                                <select class="form-control input-lg" name="id_profesional" id="id_profesional">
                                <option value="">Profesional</option>
                                    <?php require_once 'config/conexion.php';
                                        $sql = "SELECT * FROM profesionales";
                                        $query = mysqli_query($con, $sql);

                                        while ($data = mysqli_fetch_assoc($query)){
                                            $id_profesional = $data['id_profesional'];
                                            $nombresp = $data['nombresp'];  
                                    ?>
                                    <option value="<?php echo $id_profesional; ?>"><?php echo $nombresp; ?></option>
                                    <?php
                                        }
                                    ?>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-calendar-o"></i></span>
                                <select class="form-control input-lg" name="className" id="className">
                                    <option value="">Estado Cita</option>
                                    <option value="PENDIENTE">PENDIENTE</option>
                                    <option value="ASISTIO">ASISTIO</option>
                                </select>
                            </div>
                        </div>  
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Salir</button>
                    <button type="submit" class="btn btn-primary">GUARDAR</button>
                </div>
            </form>
        </div>
    </div>
</div>