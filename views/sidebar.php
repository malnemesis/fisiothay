<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <ul class="sidebar-menu" data-widget="tree">
        <li class="header">NAVEGACION PRINCIPAL</li>
        <li>
          <a href="inicio.php">
            <i class="fa fa-calendar"></i> <span>INICIO</span>
          </a>
        </li>
        <li>
          <a href="citas.php">
            <i class="fa fa-calendar-check-o"></i> <span>CITAS</span>
          </a>
        </li>
        <li>
          <a href="atenciones.php">
            <i class="fa fa-user-plus"></i> <span>ATENCIONES</span>
          </a>
        </li>
        <li>
          <a href="pacientes.php">
            <i class="fa fa-users"></i> <span>PACIENTES</span>
          </a>
        </li>
        <li>
          <a href="profesionales.php">
            <i class="fa fa-vcard"></i> <span>PROFESIONALES</span>
          </a>
        </li>
        <li>
          <a href="categorias.php">
            <i class="fa fa-window-restore"></i> <span>CATEGORIAS</span>
          </a>
        </li>
        <li>
          <a href="reportes.php">
            <i class="fa fa-line-chart"></i> <span>REPORTES</span>
          </a>
        </li>
        <li>
          <a href="usuarios.php">
            <i class="fa fa-user-circle-o"></i> <span>USUARIOS</span>
          </a>
        </li>
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>