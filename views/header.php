<header class="main-header">
    <!-- Logo -->
    <a href="index2.html" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>F</b>T</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Fisio</b>Thay</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                <li>
                    <a href="" data-toggle="modal" data-target="#logout"><i class="fa fa-sign-out"></i></a>
                </li>
            </ul>
        </div>
    </nav>
</header>

<div id="logout" class="modal fade" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form role="form" id="editar_categoria">
                <div class="modal-header" style="background:#3c8dbc; color:white">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">CERRAR SESIÓN</h4>
                </div>
                <div class="modal-body">
                    <div class="alert alert-default" role="alert">
                        <h4 class="alert-heading">SALIR!</h4>
                        ¿Seguro que quiere salir?
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancelar</button>
                    <a href="login.php?logout" class="btn btn-danger">SI, SALIR</a>
                </div>
            </form>
        </div>
    </div>
</div>