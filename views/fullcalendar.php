<!-- jQuery 3 -->
<script src="views/AdminLTE/bower_components/jquery/dist/jquery.min.js"></script>
<!-- Bootstrap 3.3.7 -->
<script src="views/AdminLTE/bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="views/AdminLTE/bower_components/jquery-ui/jquery-ui.min.js"></script>
<!-- Slimscroll -->
<script src="views/AdminLTE/bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="views/AdminLTE/bower_components/fastclick/lib/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="views/AdminLTE/dist/js/adminlte.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="views/AdminLTE/dist/js/demo.js"></script>
<!-- fullCalendar -->
<script src="views/AdminLTE/bower_components/moment/moment.js"></script>
<script src="views/AdminLTE/bower_components/fullcalendar/dist/fullcalendar.min.js"></script>
<script src="views/AdminLTE/bower_components/fullcalendar/dist/locale/es.js"></script>
<!-- Page specific script -->

