<?php

session_start();
if (!isset($_SESSION['user_login_status']) AND $_SESSION['user_login_status'] != 1) {
    header("location: login.php");
    exit;
    }
 
require_once "config/db.php";
require_once "config/conexion.php";

$sql = ("SELECT * FROM citas");
$citas = $con->query($sql);

?>

<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-calendar"></i> INICIO
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <!-- /.col -->
                    <div class="col-md-12">
                        <div class="box box-primary">
                            <div class="box-body no-padding">
                                <!-- THE CALENDAR -->
                                <div id="calendar"></div>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /. box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <?php include 'views/modal/citas/agregar_cita.php'; ?>
        <?php include 'views/modal/citas/editar_cita.php'; ?>
        <?php include 'views/modal/citas/ver_cita.php'; ?>


        <?php include 'views/footer.php'; ?>
        <?php include 'views/fullcalendar.php'; ?>

        <div class="control-sidebar-bg"></div>

    </div>
    <!-- ./wrapper -->

    <script>
    $(document).ready(function() {

        $('#calendar').fullCalendar({
            header: {
                language: 'es',
                left: 'prev,next today',
                center: 'title',
                right: 'month,basicWeek,basicDay',

            },
            editable: true,
            eventLimit: true,
            selectable: true,
            selectHelper: true,

            eventDrop: function(cita, delta, revertFunc) {
                edit(cita);
            },

            eventResize: function(cita, dayDelta, minuteDelta, revertFunc) {
                edit(cita);
            },

            events: [
                <?php foreach($citas as $cita):
                
                				$start = explode(" ", $cita['start']);
                                $end = explode(" ", $cita['end']);
                                if($start[1] == '00:00:00'){
                                    $start = $start[0];
                                }else{
                                    $start = $cita['start'];
                                }
                                if($end[1] == '00:00:00'){
                                    $end = $end[0];
                                }else{
                                    $end = $cita['end'];
                                }
                ?> {
                    id: '<?php echo $cita['id_cita']; ?>',
                    title: '<?php echo $cita['title']; ?>',
                    start: '<?php echo $start; ?>',
                    end: '<?php echo $end; ?>',
                    editable: '<?php echo $cita['editable']; ?>',
                    className: '<?php echo $cita['className']; ?>',
                },
                <?php endforeach; ?>
    
            ],

            dayClick: function(start) {
                $('#ModalAdd #start').val(moment(start).format('YYYY-MM-DD'));
                $('#ModalAdd').modal('show');

            },

            eventClick: function(cita, jsEvent, view) {
                $('#ModalEdit #id').val(cita.id);
                $('#ModalEdit #title').val(cita.title);
                $('#ModalEdit #start').val(cita.start.format('MM-DD-YYYY'));
                $('#ModalEdit #estado').val(cita.className);

                $('#Ver #id').val(cita.id);
                $('#Ver #title').val(cita.title);
                $('#Ver #start').val(cita.start.format('MM-DD-YYYY'));
                $('#Ver #estado').val(cita.className);
                $('#Ver').modal('show');
                   
            }
        });
    });
    </script>

    <script src="views/plugins/SweetAlert/sweetalert.all.js"></script>
    <script type="text/javascript" src="js/citas.js"></script>

</body>

</html>