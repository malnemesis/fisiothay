<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-window-restore"></i> CATEGORIAS
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#AgregarCategoria">
                                    AGREGAR CATEGORIA
                                </button>
                            </div>

                            <?php include 'views/modal/categorias/agregar_categoria.php'; ?>
                            <?php include 'views/modal/categorias/editar_categoria.php'; ?>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="categorias"
                                    class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>NOMBRE</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
                                        require_once 'config/db.php';
                                        require_once 'config/conexion.php'; 
                                            $sql = "SELECT * FROM categorias";
                                            $query = mysqli_query($con, $sql);

                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $id_categoria = $data['id_categoria'];
                                                    $codigo = $data['id_categoria'];
                                                    $nombre = $data['nombre'];
                                                
                                                echo "<tr>
                                                        <td>$data[nombre]</td>
                                                        <td>
                                                        <div>   
                                                       ";
                                          ?>

                                        <input type="" value="<?php echo $id_categoria;?>" id="id_categoria<?php echo $id_categoria; ?>" />
                                        <input type="" value="<?php echo $nombre;?>" id="nombre<?php echo $id_categoria;?>" />

                                        <a href="#" class='btn btn-warning' title='Editar'
                                            onclick="obtener_datos('<?php echo $id_categoria;?>');" data-toggle="modal"
                                            data-target="#Editar"><i class="fa fa-edit"></i> EDITAR
                                        </a>

                                        <?php
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                          ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/categorias.js"></script>

</body>

</html>