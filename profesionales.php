<!DOCTYPE html>
<html>

<?php require_once 'config/db.php';

include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-vcard"></i> PROFESIONALES
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#Agregar">
                                    AGREGAR PROFESIONAL
                                </button>
                            </div>

                            <?php include 'views/modal/profesionales/agregar_profesional.php'; ?>
                            <?php include 'views/modal/profesionales/editar_profesional.php'; ?>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="profesionales"
                                    class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>NOMBRES</th>
                                            <th>CÉDULA</th>
                                            <th>EMAIL</th>
                                            <th>TELÉFONO</th>
                                            <th>CATEGORÍA</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php require_once 'config/conexion.php'; 
                                            $sql = "SELECT
                                                        profesionales.id_profesional,
                                                        profesionales.nombresp,
                                                        profesionales.cedula,
                                                        profesionales.email,
                                                        profesionales.telefono,
                                                        profesionales.id_categoria,
                                                        categorias.nombre
                                                        FROM
                                                        profesionales
                                                        INNER JOIN categorias ON profesionales.id_categoria = categorias.id_categoria";
                                            $query = mysqli_query($con, $sql);

                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $id_profesional = $data['id_profesional'];
                                                    $codigo = $data['id_profesional'];
                                                    $nombresp = $data['nombresp'];
                                                    $cedula = $data['cedula'];
                                                    $email = $data['email'];
                                                    $telefono = $data['telefono'];
                                                    $id_categoria = $data['id_categoria'];
                                                    $nombre = $data['nombre'];
                            
                                                echo "<tr>
                                                        <td>$data[nombresp]</td>
                                                        <td>$data[cedula]</td>
                                                        <td>$data[email]</td>
                                                        <td>$data[telefono]</td>
                                                        <td>$data[nombre]</td>
                                                        <td>
                                                        <div>
                                                       ";
                                        ?>

                                        <input type="hidden" value="<?php echo $id_profesional;?>"id="id_profesional<?php echo $id_profesional; ?>" />
                                        <input type="hidden" value="<?php echo $nombresp;?>" id="nombresp<?php echo $id_profesional;?>" />
                                        <input type="hidden" value="<?php echo $cedula;?>" id="cedula<?php echo $id_profesional;?>" />
                                        <input type="hidden" value="<?php echo $email;?>" id="email<?php echo $id_profesional;?>" />
                                        <input type="hidden" value="<?php echo $telefono;?>" id="telefono<?php echo $id_profesional;?>" />
                                        <input type="hidden" value="<?php echo $id_categoria;?>" id="id_categoria<?php echo $id_profesional;?>" />
                                        <input type="hidden" value="<?php echo $nombre;?>" id="nombre<?php echo $id_profesional;?>" />

                                        <a href="#" class='btn btn-warning' title='Editar'
                                            onclick="obtener_datos('<?php echo $id_profesional;?>');" data-toggle="modal"
                                            data-target="#editar"><i class="glyphicon glyphicon-edit"></i> EDITAR
                                        </a>
                                        <?php
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                          ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/profesionales.js"></script>

</body>

</html>