<?php 
require_once("config/db.php");
require_once("class/Login.php");

$login = new Login();

if ($login->isUserLoggedIn() == true) {
   header("location: inicio.php");
} else {

?>

<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="login-page bg-login">
    <div class="login-box">
        <div style="color:#3c8dbc" class="login-logo">
            <img style="margin-top:-12px" src="views/favicon/favicon-32x32.png" alt="Logo" height="50"> <b>FISIOTHAY</b>
        </div><!-- /.login-logo -->
        <div class="login-box-body">
            <p class="login-box-msg"> Por favor Inicie Sesión</p>
            <form method="post" accept-charset="utf-8" action="login.php" name="loginform" autocomplete="off"
                role="form" class="form-signin">
                <?php
				
				if (isset($login)) {
					if ($login->errors) {
						?>
                <div class="alert alert-danger alert-dismissible" role="alert">
                <h4 class="alert-heading">Error!</h4>

                    <?php 
						foreach ($login->errors as $error) {
							echo $error;
						}
						?>
                </div>
                <?php
					}
					if ($login->messages) {
						?>
                <div class="alert alert-success alert-dismissible" role="alert">
                 <h4 class="alert-heading">Aviso!</h4>
                    <?php
						foreach ($login->messages as $message) {
							echo $message;
						}
						?>
                </div>
                <?php 
					}
				}
				?>
                <span id="reauth-email" class="reauth-email"></span>
                <div class="form-group has-feedback">
                    <input type="text" class="form-control" name="user_name" placeholder="Usuario">
                    <span class="fa fa-user form-control-feedback"></span>
                </div>
                <div class="form-group has-feedback">
                    <input type="password" class="form-control" name="user_password" placeholder="Contraseña"
                        autocomplete="off">
                    <span class="fa fa-lock form-control-feedback"></span>
                </div>
                <br />
                <div class="row">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-lg btn-primary btn-block btn-signin" name="login"
                            id="submit">Iniciar
                            Sesión  <i class="fa fa-sign-in"></i></button>
                    </div><!-- /.col -->
                </div>
            </form>
        </div><!-- /.login-box-body -->
    </div><!-- /.login-box -->

    <script type="text/javascript" src="js/usuarios.js"></script>
    <?php
}