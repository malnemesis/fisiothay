<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-user-plus"></i> ATENCIONES
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">

                            </div>

                            <?php include 'views/modal/atenciones/editar_atencion.php'; ?>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="atenciones"
                                    class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>NOMBRES</th>
                                            <th>DIAGNOSTICO</th>
                                            <th>TRATAMIENTO</th>
                                            <th>FECHA</th>
                                            <th>PRECIO</th>
                                            <th>ESTADO</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
                                        require_once 'config/db.php'; 
                                        require_once 'config/conexion.php'; 
                                            $sql = "SELECT
                                            atenciones.id_atencion,
                                            dcitas.id_cita,
                                            atenciones.id_dcita,
                                            atenciones.diagnostico,
                                            atenciones.tratamiento,
                                            atenciones.precio,
                                            atenciones.estado,
                                            pacientes.nombres,
                                            citas.`start`
                                            FROM
                                            atenciones
                                            INNER JOIN dcitas ON atenciones.id_dcita = dcitas.id_dcita
                                            INNER JOIN pacientes ON dcitas.id_paciente = pacientes.id_paciente
                                            INNER JOIN citas ON dcitas.id_cita = citas.id_cita
                                            ORDER BY citas.`start` DESC";
                                            $query = mysqli_query($con, $sql);

                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $id_atencion = $data['id_atencion'];
                                                    $codigo = $data['id_atencion'];
                                                    $id_cita = $data['id_cita'];
                                                    $id_dcita = $data['id_dcita'];
                                                    $diagnostico = $data['diagnostico'];
                                                    $tratamiento = $data['tratamiento'];
                                                    $precio = $data['precio'];
                                                    $estado = $data['estado'];
                                                    $nombres = $data['nombres'];
                                                    $start = $data['start'];
                                                    
                                                echo "<tr>
                                                        <td>$data[nombres]</td>
                                                        <td>$data[diagnostico]</td>
                                                        <td>$data[tratamiento]</td>
                                                        <td>$data[start]</td>
                                                        <td>$data[precio]</td>
                                                        <td>";

                                                        if ($estado =='PAGADO') {
                                                            echo "<button class='btn btn-success btn-xs'>$data[estado]</button>";
                                                        }elseif ($estado =='PROMO') {
                                                            echo "<button class='btn btn-warning btn-xs'>$data[estado]</button>";
                                                        }else{
                                                            echo "<button class='btn btn-danger btn-xs'>$data[estado]</button>";
                                                        }

                                                echo "  <td>
                                                        <div>
                                                       ";
                                            ?>

                                        <input type="hidden" value="<?php echo $id_atencion;?>"
                                            id="id_paciente<?php echo $id_atencion; ?>" />
                                        <input type="hidden" value="<?php echo $id_cita;?>"
                                            id="id_cita<?php echo $id_atencion; ?>" />
                                        <input type="hidden" value="<?php echo $id_dcita;?>"
                                            id="id_dcita<?php echo $id_atencion; ?>" />
                                        <input type="hidden" value="<?php echo $nombres;?>"
                                            id="nombres<?php echo $id_atencion;?>" />
                                        <input type="hidden" value="<?php echo $diagnostico;?>"
                                            id="diagnostico<?php echo $id_atencion;?>" />
                                        <input type="hidden" value="<?php echo $tratamiento;?>"
                                            id="tratamiento<?php echo $id_atencion;?>" />
                                        <input type="hidden" value="<?php echo $precio;?>"
                                            id="precio<?php echo $id_atencion;?>" />
                                        <input type="hidden" value="<?php echo $estado;?>"
                                            id="estado<?php echo $id_atencion;?>" />

                                        <a class='btn btn-warning' title='Editar'
                                            onclick="obtener_datos_atencion('<?php echo $id_atencion;?>');"
                                            data-toggle="modal" data-target="#Editar"><i
                                                class="glyphicon glyphicon-edit"></i> EDITAR
                                        </a>

                                        <?php
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                          ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/atenciones.js"></script>

</body>

</html>