<?php require_once 'config/db.php'; ?>

<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-calendar-check-o"></i> CITAS
                </h1>

            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">

                            </div>

                            <?php include 'views/modal/citas/agregar_cita.php'; ?>
                            <?php include 'views/modal/atenciones/agregar_atencion.php'; ?>


                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="citas" class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>PACIENTE</th>
                                            <th>PROFESIONAL</th>
                                            <th>ESTADO</th>
                                            <th>FECHA</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php 
                                       
                                        require_once 'config/conexion.php';  
                                            $sql = "SELECT
                                                        dcitas.id_dcita,
                                                        pacientes.nombres,
                                                        profesionales.nombresp,
                                                        citas.`start`,
                                                        citas.`end`,
                                                        citas.className
                                                    FROM
                                                        dcitas
                                                        INNER JOIN citas ON dcitas.id_cita = citas.id_cita
                                                        INNER JOIN profesionales ON dcitas.id_profesional = profesionales.id_profesional
                                                        INNER JOIN pacientes ON dcitas.id_paciente = pacientes.id_paciente
                                                    ORDER BY citas.`start` DESC";
                                            $query = mysqli_query($con, $sql);

                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $id_dcita = $data['id_dcita'];
                                                    $codigo = $data['id_dcita'];
                                                    $paciente = $data['nombres'];
                                                    $profesional = $data['nombresp'];
                                                    $start = $data['start'];
                                                    $end = $data['end'];
                                                    $estado = $data['className'];
                                                    $hoy = date("Y-m-d 00:00:00");
                                                    
                                                
                                                echo "<tr>
                                                        <td>$data[nombres]</td>
                                                        <td>$data[nombresp]</td>
                                                        <td>$data[className]</td>                                       
                                                        <td>";
                                                        if ($start == $hoy) {
                                                            echo "<button class='btn btn-warning'>$data[start]</butto>";
                                                            $boton = 'btn btn-warning'; 
                                                        }elseif($estado == 'PENDIENTE'){
                                                            echo "<button class='btn btn-primary btn-xs'>$data[start]</button>";
                                                            $boton = 'btn btn-warning';
                                                        }elseif($estado == 'PAGADO'){
                                                            echo "<button class='btn btn-success btn-xs'>$data[start]</button>";
                                                            $boton = 'btn btn-warning hidden';
                                                        }elseif($estado == 'DEBE'){
                                                            echo "<button class='btn btn-danger btn-xs'>$data[start]</button>";
                                                            $boton = 'btn btn-warning hidden';
                                                        }elseif($estado == 'ASISTIO'){
                                                            echo "<button class='btn btn-success btn-xs'>$data[start]</button>";
                                                            $boton = 'btn btn-warning';
                                                        }else{
                                                            echo "<button class='btn btn-default btn-xs'>$data[start]</button>";
                                                        }
                                                      
                     
                                        echo " <td>
                                            <div>
                                                ";


                                                ?>

                                        <input type="hidden" value="<?php echo $id_dcita;?>"
                                            id="id_dcita<?php echo $id_dcita; ?>" />
                                        <input type="hidden" value="<?php echo $paciente;?>"
                                            id="paciente<?php echo $id_dcita;?>" />
                                        <input type="hidden" value="<?php echo $start;?>"
                                            id="start<?php echo $id_dcita;?>" />
                                        <input type="hidden" value="<?php echo $end;?>"
                                            id="end<?php echo $id_dcita;?>" />
                                        <input type="hidden" value="<?php echo $profesional;?>"
                                            id="profesional<?php echo $id_dcita;?>" />
                                        <input type="hidden" value="<?php echo $estado;?>"
                                            id="estado<?php echo $id_dcita;?>" />
                                                
                                        <a class='<?php echo $boton; ?>' title='Agregar' onclick="obtener_datos('<?php echo $id_dcita;?>');"data-toggle="modal"
                                            data-target="#Agregar"><i class='fa fa-user-plus'></i> ATENCIÓN</a>
                                       
                                        <?php
                                    
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/citas.js"></script>
    <script type="text/javascript" src="js/atenciones.js"></script>

</body>

</html>