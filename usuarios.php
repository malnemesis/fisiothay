<?php require_once 'config/db.php'; ?>

<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-user-circle-o"></i> USUARIOS
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#Agregar">
                                    AGREGAR USUARIO
                                </button>
                            </div>

                            <?php include 'views/modal/usuarios/agregar_usuario.php'; ?>
                            <?php include 'views/modal/usuarios/editar_usuario.php'; ?>
                            <?php include 'views/modal/usuarios/password_usuario.php'; ?>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="usuarios"
                                    class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>NOMBRES</th>
                                            <th>USUARIO</th>
                                            <th>EMAIL</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php require_once 'config/conexion.php'; 
                                            $sql = "SELECT * FROM users";
                                            $query = mysqli_query($con, $sql);

                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $user_id = $data['user_id'];
                                                    $codigo = $data['user_id'];
                                                    $firstname = $data['firstname'];
                                                    $lastname = $data['lastname'];
                                                    $user_name = $data['user_name'];
                                                    $user_email = $data['user_email'];
                                            
                                                echo "<tr>
                                                        <td>$data[firstname] $data[lastname]</td>
                                                        <td>$data[user_name]</td>
                                                        <td>$data[user_email]</td>
                                                        <td>
                                                        <div>
                                                       ";
                                        ?>

                                        <input type="hidden" value="<?php echo $user_id;?>"
                                            id="user_id<?php echo $user_id; ?>" />
                                        <input type="hidden" value="<?php echo $firstname;?>" id="nombres<?php echo $user_id;?>" />
                                        <input type="hidden" value="<?php echo $lastname;?>" id="apellidos<?php echo $user_id;?>" />
                                        <input type="hidden" value="<?php echo $user_name;?>" id="usuario<?php echo $user_id;?>" />
                                        <input type="hidden" value="<?php echo $user_email;?>" id="email<?php echo $user_id;?>" />
                            
                                        <a href="#" class='btn btn-warning' title='Editar Usuario'
                                            onclick="obtener_datos('<?php echo $user_id;?>');" data-toggle="modal"
                                            data-target="#editar"><i class="fa fa-edit"></i>
                                        </a>
                                        <a href="#" class='btn btn-info' title='Cambiar Contraseña'
                                            onclick="obtener_datos_pas('<?php echo $user_id;?>');" data-toggle="modal"
                                            data-target="#editarpas"><i class="fa fa-gear"></i>
                                        </a>
                                        <?php
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                          ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/usuarios.js"></script>

</body>

</html>