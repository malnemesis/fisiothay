<?php require_once 'config/db.php'; ?>

<!DOCTYPE html>
<html>

<?php include 'views/head.php'; ?>

<body class="hold-transition skin-blue sidebar-mini">
    <div class="wrapper">

        <?php include 'views/header.php'; ?>

        <!-- Left side column. contains the logo and sidebar -->

        <?php include 'views/sidebar.php'; ?>

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <section class="content-header">
                <h1>
                    <i class="fa fa-users"></i> PACIENTES
                </h1>
            </section>

            <!-- Main content -->
            <section class="content">
                <div class="row">
                    <div class="col-xs-12">
                        <!-- /.box -->
                        <div class="box">
                            <div class="box-header with-border">
                                <button class="btn btn-primary" data-toggle="modal" data-target="#modalAgregarPaciente">
                                    AGREGAR PACIENTE
                                </button>
                            </div>

                            <?php include 'views/modal/pacientes/agregar_paciente.php'; ?>
                            <?php include 'views/modal/pacientes/editar_paciente.php'; ?>

                            <!-- /.box-header -->
                            <div class="box-body">
                                <table id="pacientes"
                                    class="table table-bordered table-striped table-hover dt-responsive">
                                    <thead>
                                        <tr>
                                            <th>NOMBRES</th>
                                            <th>GENERO</th>
                                            <th>EDAD</th>
                                            <th>TELÉFONO</th>
                                            <th>DIRECCIÓN</th>
                                            <th>ACCIONES</th>
                                        </tr>
                                    </thead>
                                    <tbody>

                                        <?php require_once 'config/conexion.php'; 
                                            $sql = "SELECT id_paciente,nombres,genero,edad,telefono,direccion FROM pacientes";
                                            $query = mysqli_query($con, $sql);
                                            
                                            while ($data = mysqli_fetch_assoc($query)){
                                                    $id_paciente = $data['id_paciente'];
                                                    $codigo = $data['id_paciente'];
                                                    $nombres = $data['nombres'];
                                                    $genero = $data['genero'];
                                                    $edad = $data['edad'];
                                                    $telefono = $data['telefono'];
                                                    $direccion = $data['direccion'];
                            
                                                echo "<tr>
                                                        <td>$data[nombres]</td>
                                                        <td>$data[genero]</td>
                                                        <td>$data[edad]</td>
                                                        <td>$data[telefono]</td>
                                                        <td>$data[direccion]</td>
                                                        <td>
                                                        <div>
                                                       ";
                                          ?>

                                        <input type="hidden" value="<?php echo $id_paciente;?>"
                                            id="id_paciente<?php echo $id_paciente; ?>" />
                                        <input type="hidden" value="<?php echo $nombres;?>" id="nombres<?php echo $id_paciente;?>" />
                                        <input type="hidden" value="<?php echo $genero;?>" id="genero<?php echo $id_paciente;?>" />
                                        <input type="hidden" value="<?php echo $edad;?>" id="edad<?php echo $id_paciente;?>" />
                                        <input type="hidden" value="<?php echo $telefono;?>" id="telefono<?php echo $id_paciente;?>" />
                                        <input type="hidden" value="<?php echo $direccion;?>" id="direccion<?php echo $id_paciente;?>" />
                                    
                                        <a href="#" class='btn btn-warning' title='Editar'
                                            onclick="obtener_datos('<?php echo $id_paciente;?>');" data-toggle="modal"
                                            data-target="#editar"><i class="glyphicon glyphicon-edit"></i> EDITAR
                                        </a>
                            
                                        <?php
                                            echo "    </div>
                                                    </td>
                                                  </tr>";
                                        
                                          }
                                          ?>
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.box-body -->
                        </div>
                        <!-- /.box -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </section>
            <!-- /.content -->
        </div>

        <!-- /.content-wrapper -->

        <?php include 'views/footer.php' ?>
        <?php include 'views/components.php' ?>

        <div class="control-sidebar-bg"></div>
    </div>
    <!-- ./wrapper -->

    <script type="text/javascript" src="js/pacientes.js"></script>

</body>

</html>