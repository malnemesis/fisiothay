$(document).ready(function() {

    $("input").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })

});

$("#datos_cita").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/citas/nueva_cita.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "CITA registrada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#3c8dbc",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("citas.php");
                }
            })
        }
    });
    event.preventDefault();
})

function edit(cita) {
    start = cita.start.format('YYYY-MM-DD HH:mm:ss');
    if (cita.end) {
        end = cita.end.format('YYYY-MM-DD HH:mm:ss');
    } else {
        end = start;
    }

    id = cita.id;

    Event = [];
    Event[0] = id;
    Event[1] = start;
    Event[2] = end;

    $.ajax({
        type: "POST",
        url: 'php/ajax/citas/editar_cita.php',
        data: {
            Event: Event
        },
        success: function(datos) {
            swal({
                type: "success",
                title: "CITA editada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#ffc107",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("inicio.php");
                }
            })
        }
    });
}

$(function() {
    $('#citas').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "order": [
            [0, "desc"]
        ],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,

    })
})