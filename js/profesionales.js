$(document).ready(function() {

    $("input").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })

});

$("#datos_profesional").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/profesionales/nuevo_profesional.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "PROFESIONAL registrado exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#3c8dbc",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("profesionales.php");
                }
            })
        }
    });
    event.preventDefault();
})

function obtener_datos(id) {
    var codigo = $("#codigo" + id).val();
    var nombresp = $("#nombresp" + id).val();
    var cedula = $("#cedula" + id).val();
    var email = $("#email" + id).val();
    var telefono = $("#telefono" + id).val();
    var id_categoria = $("#id_categoria" + id).val();

    $("#mod_id").val(id);
    $("#mod_codigo").val(codigo);
    $("#mod_nombres").val(nombresp);
    $("#mod_cedula").val(cedula);
    $("#mod_email").val(email);
    $("#mod_telefono").val(telefono);
    $("#mod_id_categoria").val(id_categoria);

}

$("#editar_profesional").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/profesionales/editar_profesional.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "PROFESIONAL editado exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#ffc107",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("profesionales.php");
                }
            })
        }

    });
    event.preventDefault();
})

$(function() {
    $('#profesionales').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "order": [
            [0, "desc"]
        ],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,

    })
})