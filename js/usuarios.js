$("#datos_usuario").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/usuarios/nuevo_usuario.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "USUARIO registrado exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#3c8dbc",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("usuarios.php");
                }
            })
        }

    });
    event.preventDefault();
})

function obtener_datos_pas(id) {
    $("#user_id_mod").val(id);
}

function obtener_datos(id) {
    var nombres = $("#nombres" + id).val();
    var apellidos = $("#apellidos" + id).val();
    var usuario = $("#usuario" + id).val();
    var email = $("#email" + id).val();

    $("#mod_id").val(id);
    $("#firstname2").val(nombres);
    $("#lastname2").val(apellidos);
    $("#user_name2").val(usuario);
    $("#user_email2").val(email);

}

$("#editar_usuario").submit(function(event) {

    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/usuarios/editar_usuario.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "USUARIO editado exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#ffc107",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("usuarios.php");
                }
            })
        }

    });
    event.preventDefault();
})

$("#editar_password").submit(function(event) {

    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/usuarios/editar_password.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "PASSWORD editado exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#17a2b8",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("usuarios.php");
                }
            })
        }
    });
    event.preventDefault();
})

$(function() {
    $('#usuarios').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "order": [
            [0, "desc"]
        ],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,

    })
})