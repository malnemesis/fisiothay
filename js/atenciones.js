$(document).ready(function() {

    $("input").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })

});

$("#datos_atencion").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/atenciones/nueva_atencion.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "ATENCION registrada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#3c8dbc",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("atenciones.php");
                }
            })
        }
    });
    event.preventDefault();
})

function obtener_datos(id) {
    var codigo = $("#codigo" + id).val();
    var paciente = $("#paciente" + id).val();
    var profesional = $("#profesional" + id).val();

    $("#id").val(id);
    $("#codigo").val(codigo);
    $("#paciente").val(paciente);
    $("#profesional").val(profesional);

}

function obtener_datos_atencion(id) {
    var codigo = $("#codigo" + id).val();
    var id_cita = $("#id_cita" + id).val();
    var id_dcita = $("#id_dcita" + id).val();
    var nombres = $("#nombres" + id).val();
    var diagnostico = $("#diagnostico" + id).val();
    var tratamiento = $("#tratamiento" + id).val();
    var precio = $("#precio" + id).val();
    var estado = $("#estado" + id).val();

    $("#id").val(id);
    $("#codigo").val(codigo);
    $("#id_cita").val(id_cita);
    $("#id_dcita").val(id_dcita);
    $("#nombres").val(nombres);
    $("#diagnostico").val(diagnostico);
    $("#tratamiento").val(tratamiento);
    $("#precio").val(precio);
    $("#estado").val(estado);

}

$("#editar_atencion").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/atenciones/editar_atencion.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "ATENCION editada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#ffc107",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("atenciones.php");
                }
            })
        }
    });
    event.preventDefault();
})

$(function() {
    $('#atenciones').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "order": [
            [0, "desc"]
        ],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,

    })
})