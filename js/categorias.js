$(document).ready(function() {

    $("input").on("keypress", function() {
        $input = $(this);
        setTimeout(function() {
            $input.val($input.val().toUpperCase());
        }, 50);
    })

});

$("#datos_categoria").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/categorias/nueva_categoria.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "CATEGORIA registrada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#3c8dbc",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("categorias.php");
                }
            })
        }
    });
    event.preventDefault();
})

function obtener_datos(id) {
    var codigo = $("#codigo" + id).val();
    var nombre = $("#nombre" + id).val();

    $("#mod_id").val(id);
    $("#mod_codigo").val(codigo);
    $("#mod_nombre").val(nombre);
}

$("#editar_categoria").submit(function(event) {
    var parametros = $(this).serialize();
    $.ajax({
        type: "POST",
        url: "php/ajax/categorias/editar_categoria.php",
        data: parametros,
        success: function(datos) {
            swal({
                type: "success",
                title: "CATEGORIA editada exitosamente.",
                showConfirmButton: true,
                confirmButtonColor: "#ffc107",
                confirmButtonText: "OK",
                closeOnConfirm: false
            }).then(function(result) {
                if (result.value) {
                    window.location.replace("categorias.php");
                }
            })
        }

    });
    event.preventDefault();
})

$(function() {
    $('#categorias').DataTable({
        language: {
            "decimal": "",
            "emptyTable": "No hay información",
            "info": "Mostrando _START_ a _END_ de _TOTAL_ Entradas",
            "infoEmpty": "Mostrando 0 to 0 of 0 Entradas",
            "infoFiltered": "(Filtrado de _MAX_ total entradas)",
            "infoPostFix": "",
            "thousands": ",",
            "lengthMenu": "Mostrar _MENU_ Entradas",
            "loadingRecords": "Cargando...",
            "processing": "Procesando...",
            "search": "Buscar:",
            "zeroRecords": "Sin resultados encontrados",
            "paginate": {
                "first": "Primero",
                "last": "Ultimo",
                "next": "Siguiente",
                "previous": "Anterior"
            }
        },
        "order": [
            [0, "desc"]
        ],
        'paging': true,
        'lengthChange': true,
        'searching': true,
        'ordering': true,
        'info': true,
        'autoWidth': false,

    })
})